
class GameField {
    
    state = [           // поле игры
        ["", "", ""],
        ["", "", ""],
        ["", "", ""]
    ];
    mode = "x";         // кто ходит
    isOverGame = false; // флаг окончания игры
    isDrawnGame = false; // флаг ничьи
    
    
        
    
    
    setMode(mode){          // метод для изменения активного игрока
        this.mode = mode == "x" ? "o" : "x";
    }
    
    FieldCellValue(state){  // метод для заполнения клетки поле
        this.state = state;
        let checkingInput = false; //проверка ввода
        
        while(!checkingInput){       // ввод координат и проверка на ввод
            let enterCoord = prompt("Введите без пробела номер строки и столбца", "")
            let correctInput = /^[0-2]+$/g; // правило ввода
            if (correctInput.test(enterCoord) && state[enterCoord[0]][enterCoord[1]] == ""){
                state[enterCoord[0]][enterCoord[1]] = this.mode;
                checkingInput = true;
            } else {
            alert('Вы ввели неправильные значения')
            }   
        }
    }   

    getGameFieldStatus() {  //метод для получения информации о состоянии игрового поля
        console.table(this.state);
        

        let srtX = [0, 0, 0];   // количество заполнений в каждой строке х

        let srtO = [0, 0, 0];   // количество заполнений в каждой строке y
        
        let stX = [0, 0, 0];    // количество заполнений в каждом столбце x

        let stO = [0, 0, 0];    // количество заполнений в каждом столбце y

            
        let dx1 = 0;            // количество заполнений в 1 диагонали x
        let dx2 = 0;            // количество заполнений во 2 диагонали x

        let do1 = 0;            // количество заполнений в 1 диагонали y         
        let do2 = 0;            // количество заполнений во 2 диагонали y
        
        let empty = 9;          // количество пустых полей
        
        for (let i = 0 ; i < 3; i++){       //подсчет заполнений на поле
            for (let j = 0; j < 3; j++){
                if (this.state[i][j] == this.mode){  
                    srtX[i]++;
                    stX[j]++;
                    empty -= 1;
                    if (i == j)
                    {
                        dx1++;
                    }
                    if (i + j == 2)
                    {
                        dx2++;
                    }
                }
                if (this.state[i][j] == this.mode){  
                    srtO[i]++;
                    stO[j]++;
                    empty -= 1;
                    if (i == j)
                    {
                        do1++;
                    }
                    if (i + j == 2)
                    {
                        do2++;
                    }
                }
                
            }
            
        }
        function victory(arr){

            for (let i = 0; i < 3; i++){
                if (arr[i] === 3){
                    return true;
                }
            }
        }
        
    
        if (victory(srtX) || victory(stX) || dx1 == 3 || dx2 == 3){         //проверка х на победу
            this.isOverGame = true;
        } else if (victory(srtO) || victory(stO) || do1 == 3 || do2 == 3){  //проверка o на победу
            this.isOverGame = true;
        } 
        if (empty <= 0){                                                    //проверка на ничью
            this.isOverGame = true;
            this.isDrawnGame = true;
        }
        

        

    }
    restart() {
        if (this.isOverGame == true || this.isDrawnGame == true){
            this.state =[           
            ["", "", ""],
            ["", "", ""],
            ["", "", ""]
            ];
            this.isOverGame = false; 
            this.isDrawnGame = false; 
            this.setMode('x');
            
        }

    }

}

const gameField = new GameField();


while(!gameField.isOverGame){
    gameField.FieldCellValue(gameField.state, gameField.mode);
    gameField.getGameFieldStatus();
    if (gameField.isDrawnGame){
        alert("Ничья")
        gameField.restart();
    } if (gameField.isOverGame){
       alert(`Победил: ${gameField.mode}`)
       gameField.restart();
    }
    gameField.setMode(gameField.mode);
}















