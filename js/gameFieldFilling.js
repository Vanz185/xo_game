class GameField {
    
    state = [           // поле игры
        ["", "", ""],
        ["", "", ""],
        ["", "", ""]
    ];
    mode = "x";         // кто ходит
    isOverGame = false; // флаг окончания игры
    isDrawnGame = false; // флаг ничьи

    
    
    getGameFieldStatus(cell) {  //метод для получения информации о состоянии игрового поля
        

        let srtX = [0, 0, 0];   // количество заполнений в каждой строке х

        let srtO = [0, 0, 0];   // количество заполнений в каждой строке y
        
        let stX = [0, 0, 0];    // количество заполнений в каждом столбце x

        let stO = [0, 0, 0];    // количество заполнений в каждом столбце y

            
        let dx1 = 0;            // количество заполнений в 1 диагонали x
        let dx2 = 0;            // количество заполнений во 2 диагонали x

        let do1 = 0;            // количество заполнений в 1 диагонали y         
        let do2 = 0;            // количество заполнений во 2 диагонали y
        
        let empty = 9;          // количество пустых полей
        
        for (let i = 0 ; i < 3; i++){       //подсчет заполнений на поле
            for (let j = 0; j < 3; j++){
                if (this.state[i][j] == 'x'){  
                    srtX[i]++;
                    stX[j]++;
                    empty -= 1;
                    if (i == j)
                    {
                        dx1++;
                    }
                    if (i + j == 2)
                    {
                        dx2++;
                    }
                }
                if (this.state[i][j] == 'o'){  
                    srtO[i]++;
                    stO[j]++;
                    empty -= 1;
                    if (i == j)
                    {
                        do1++;
                    }
                    if (i + j == 2)
                    {
                        do2++;
                    }
                }
                
            }
            
        }
        
        function victory(arr){

            for (let i = 0; i < 3; i++){
                if (arr[i] === 3){
                    return true;
                }
            }
        }
        
    
        if (victory(srtX) || victory(stX) || dx1 == 3 || dx2 == 3){         //проверка х на победу
            this.isOverGame = true;
        } else if (victory(srtO) || victory(stO) || do1 == 3 || do2 == 3){  //проверка o на победу
            this.isOverGame = true;
        } 
        if (empty <= 0){                                                    //проверка на ничью
            
            this.isDrawnGame = true;
        }

        
        if (this.isOverGame){
            alert(`Победил: ${this.mode}`);
            
        } else if (this.isDrawnGame){
            alert("Ничья");
        } 

        

    }
    
    
    setMode(mode){          // метод для изменения активного игрока
        this.mode = mode == "x" ? "o" : "x";
    }
    
    FieldCellValue(state, tableField, i, j){  // метод для заполнения клетки поле
        this.state = state;
        
        if (this.mode == "x"){
            let imageX = document.createElement('img');
            imageX.src = "./media/xxl-x.svg";
            tableField.appendChild(imageX);
            state[i][j] = this.mode;

        } else {
            let imageO = document.createElement('img');
            imageO.src = "./media/xxl-zero.svg";
            tableField.appendChild(imageO);
            state[i][j] = this.mode;
        }
            
    }

    restart(cell) {
        if (this.isOverGame == true || this.isDrawnGame == true){
            for (let i = 0; i < 9; i++){
                cell[i].innerHTML = '';
            }
            this.state =[           
            ["", "", ""],
            ["", "", ""],
            ["", "", ""]
            ];
            this.isOverGame = false; 
            this.isDrawnGame = false; 
            this.setMode('o');
            
        }

    }
    
    


    
}

const gameField = new GameField();


let cell = document.querySelectorAll('.cell'); 
let cellArr = [].slice.call(cell);
let tableField = [];
for (let i = 3; i > 0; i--) {
    tableField.push(cellArr.splice(0, Math.ceil(cellArr.length / i)));
}

let motion = document.querySelector('.motion');



for (let i = 0; i < 3; i++){
    for (let j = 0; j < 3; j++){
        tableField[i][j].onclick = function (){
            if (tableField[i][j].innerHTML === ''){
                console.log(tableField)
                gameField.FieldCellValue(gameField.state, tableField[i][j], i, j);
                gameField.getGameFieldStatus();
                gameField.setMode(gameField.mode);
                
                gameField.restart(cell);
                if (gameField.mode == 'x'){
                    motion.src = './media/x.svg';
                } else motion.src = './media/zero.svg';
                
            } else alert('Данная клетка занята');
        }
    }
}


let cell1 = document.querySelectorAll('.cell'); 
let cellArr1 = [].slice.call(cell1);
cellArr1.onclick = function(){alert('да')}



