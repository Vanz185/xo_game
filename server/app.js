const express = require('express');
const authRouter = require('./routes/auth')
const ratingRouter = require('./routes/rating')
const PORT = process.env.PORT || 3001
const app = express();
const morgan = require('morgan');
const methodOverride = require('method-override');


app.use(express.urlencoded({ extended: false }));
app.set('view engine', 'ejs');
app.use(express.static(__dirname + '/public'));
app.use(morgan(':method :url :status :res[content-length] - :response-time ms'));
app.use(methodOverride('_method'));

app.get('/', (req, res) => {
    res.redirect('auth')
})


app.use(authRouter);
app.use(ratingRouter);



app.listen(PORT);