const express = require('express');
const router = express.Router();
const crypto = require('crypto');
const { error } = require('console');

router.get('/auth', (req, res) => {
    res.render('../view/auth.ejs');
})

const algorithm = 'aes-256-ctr'
const secretKey = 'vOVH6sdmpNWjRRIqCc7rdxs01lwHzfr3'


const decrypt = hash => {
  const decipher = crypto.createDecipheriv(algorithm, secretKey, Buffer.from(hash.iv, 'hex'))

  const decrpyted = Buffer.concat([decipher.update(Buffer.from(hash.content, 'hex')), decipher.final()])

  return decrpyted.toString()
}

const users = [
    {
        login:1234,
        password:{iv: 'eb15a4d79a8faf8588ae64a9c6257ca9', content: '4640eceb'}
    }
]


router.post('/auth', (req, res) =>{
    const {login, password} = req.body;
    if(users[0].login == login && decrypt(users[0].password) == password) {res.redirect('rating')}
    else res.send('Неправильный логин или пароль')
    
    
})


module.exports = router;